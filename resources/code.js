const text_input = document.querySelector('#text-input'),
saythis_btn = document.querySelector('#saythis-btn')
tts_audio = document.querySelector('#tts-audio'),
modal_body = document.querySelector('#modal').querySelector('.modal-body'),
$modal = jQuery('#modal');
tts_audio.onended = () => { saythis_btn.classList.remove('disabled'); }
function say_this() {
    saythis_btn.classList.add('disabled');
    axios.post('/tts', { text: text_input.value }).then((res) => {
        if(res.data.audio != undefined){
            tts_audio.src = `data:audio/mp3;base64,${res.data.audio}`;
            tts_audio.play();
        }
        else{
            modal_body.innerHTML = res.data.error;
            $modal.modal('show');
            saythis_btn.classList.remove('disabled');
            text_input.value = '';
        }
    });
}
# GCS - Text To Speech App

This is an express app that allows the user to hear text synthesized to speech by using the Google Cloud Text-To-Speech API

## Requirements

You will need to have add 2 things to the root of the app.
1. .env file
2. Service Account JSON file ie gcs-account-details.json, [get it here](https://console.cloud.google.com/apis/api/texttospeech.googleapis.com/credentials)

.env example
```bash
GOOGLE_APPLICATION_CREDENTIALS=gcs-account-details.json
EXPRESS_PORT=3000
```

## Installation

```bash
npm install
npm run start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
const gcs_tts = new(require('@google-cloud/text-to-speech')).TextToSpeechClient();
const rateLimit = require("express-rate-limit"),
limiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour
  max: 60, // limit each IP to 10 requests per windowMs
  handler: (req, res) => {
    res.status(200).json({error: "Your hourly quota has been reached."})
  },
  skip: (req, res) => {
    const inputText = req.body.text;
    if(inputText.length > 0 && inputText.length <= 100){
      const cacheKey = md5(inputText).toString(),
      cachedAudio = cache.get(cacheKey);
      if(cachedAudio == undefined){
        return false; //return false only if the data is coming from Google Cloud
      }
    }
    return true;
  }
})
const express = require('express');
const app = express();
const dotenv = require("dotenv").config();
const path = require('path');
const md5 = require('crypto-js/md5');
const cache = new (require( "node-cache" ))(), cache_ttl = 60 * 60 * 24;
const voice = { languageCode: 'en-AU', name: `en-AU-Wavenet-D`, ssmlGender: 'MALE' } //Awesome voice!
const textToSpeech = (text) => {
  return new Promise((resolve) => {
    gcs_tts.synthesizeSpeech({
      input: { text: text },
      voice: voice,
      audioConfig: { audioEncoding: 'MP3' },
    }).then((response) => {
      resolve(response[0].audioContent);
    })
  })
}

app.set('trust proxy', 1);
app.use(express.json());

app.use('/resources', express.static('resources'))

app.get('/', (req, res) => {
  res.sendFile(path.resolve('./html/index.html'));
});

app.post('/tts', limiter, (req, res) => {
  const inputText = req.body.text;
  if(inputText.length > 0 && inputText.length <= 100){
    const cacheKey = md5(inputText).toString(),
    cachedAudio = cache.get(cacheKey);
    if(cachedAudio == undefined){
      textToSpeech(inputText).then((audio) => {
        cache.set(cacheKey, audio, cache_ttl);
        res.status(200).json({audio: audio.toString('base64')})
      }).catch((err) => {
        console.error(err);
        res.status(200).json({error: "GCS returned an error, please try again later."})
      });
    }
    else{
      res.status(200).json({audio: cachedAudio.toString('base64')})
    }
  }
  if(inputText.length == 0){
    res.status(200).json({error: "Please enter a sentence to be synthesized to speech."})
  }
  else if(inputText.length > 100){
    res.status(200).json({error: "Only up to 100 characters are allowed at a time."})
  }
});

app.listen(dotenv.parsed.EXPRESS_PORT, () => {
  console.log("Server started!")
});